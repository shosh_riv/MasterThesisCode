###### Supplementary functions for VOlCorn GCMS processing with eRah

####Functions for getting retention indices from pubchem####
###written by Linnea Smith, iDiv, 2022

#### function to put dashes into CAS numbers ####
make_cas <- function(casnum){
  len <- nchar(casnum)
  dashes <- ifelse(len > 3,
                   paste(substr(casnum,1,len-3),substr(casnum,len-2,len-1),substr(casnum,len,len),sep="-"),
                   casnum) # if there are only three numbers, don't put dashes in - just return the input number
  return(dashes)
}

#### calculate KRI based on alkane standards (formula: https://webbook.nist.gov/chemistry/gc-ri/#REF2) ####
kri_calc <- function(compound_RT,stds,isothermal=F){
  # compound_RT can be either a single RT or a vector of RTs
  # stds is the dataframe of alkane standard RTs, example format: data.frame(C=20:10,
  #                                   RT=c(8.280,7.416,6.615,5.894,5.247,4.670,4.141,3.631,3.109,2.567,2.090),
  #                                   KRI=seq(from=2000,to=1000,by=-100))
  # (column called "C" for the number of carbons, "RT" for the measured retention times, "KRI" for the assigned retention index)
  # isothermal is whether the measurement was isothermal or not (default to F)
  
  for(i in 1:length(compound_RT)){
    tx <- compound_RT[i]
    if(tx < min(stds$RT) | tx > max(stds$RT)){ # if the RT is outside the range of the std RTs, return NA
      I_i <- NA
      if(!exists("KRI")) KRI <- I_i
      else KRI <- c(KRI,I_i)
      next
    }
    tn <- max(stds[stds$RT < tx,"RT"]) # std RT immediately below the compound RT
    tn1 <- min(stds[stds$RT > tx,"RT"]) # std RT immediately above the compound RT
    n <- stds[which(stds$RT == tn),"C"] # number of carbons in the alkane immediately below the compound
    
    if(isothermal){ # calculate isothermal RT
      I_i <- 100 * n + 100 * ( (log(tx) - log(tn)) / (log(tn1) - log(tn)) )
    } else if(!isothermal){ # calculate non-isothermal RT
      I_i <- 100 * n + 100 * ( (tx - tn) / (tn1 - tn) )
    }
    if(!exists("KRI")) KRI <- I_i
    else KRI <- c(KRI,I_i)
  }
  
  return(KRI)
}

#### calculate the dot product between two spectra ####
dotproduct <- function(x,y, 
                       x.mz=1, x.intensity=2, # set which columns of your input df contain mz and intensity
                       y.mz=1, y.intensity=2, # (default to first and second column)
                       from=38, to=450){ 
  mzrange <- seq(from=from, to=to, by=1)
  x2 <- sapply(mzrange, function (i) {
    dif <- abs(round(x[,x.mz])-i)
    res <- 0
    if(any(dif<0.5)){
      res <- x[which(dif==min(dif)),x.intensity]
    }
    return(res)
  })
  y2 <- sapply(mzrange,function(i){
    dif <- abs(round(y[,y.mz])-i)
    res <- 0
    if(any(dif<0.5)){
      res <- y[which(dif==min(dif)),y.intensity]
    }
    return(res)
  })
  as.vector((x2 %*% y2) / (sqrt(sum(x2^2)) * sqrt(sum(y2^2))))
}

### Function to read in and extract desired metadata from MSP file
msp_to_df <- function(path_to_msp,filetype="MSP",rdata_object,desired_metadata,unique_entries=T){
  ## read in raw .MSP file containing metadata and format as dataframe
  if(filetype=="MSP"){ # if your database is stored as an MSP file, read it in
    database <- data.table::fread(path_to_msp, sep=NULL,header=F,stringsAsFactors=F,blank.lines.skip=T,showProgress=T,data.table=F)
  } else if(filetype=="RData"){ # if you've stored it as an RData object, read this in before running the function and call it here
    database <- rdata_object
  }
  
  # separate into two columns by the colon (:)
  database <- data.frame(variable = data.table::tstrsplit(database[1:nrow(database),],split=":")[[1]],
                         value=data.table::tstrsplit(database[1:nrow(database),],split=":")[[2]])
  # make all variable names uppercase, so that you don't have to worry about differing conventions across libraries
  desired_metadata <- toupper(desired_metadata)
  database$variable <- toupper(database$variable)
  
  # give each compound a unique ID number
  name_rows <- which(database$variable=="NAME")
  database[name_rows,"internal_id"] <- 1:length(name_rows) # give each name an ID number
  
  # dataframe of only the metadata that you're interested in
  database.red <- database[database$variable %in% desired_metadata,]
  
  # assign the other metadata the same internal ID number as the name of the compound they all belong to
  for(i in 1:nrow(database.red)){
    if(is.na(database.red[i,"internal_id"])){
      # because each new compound starts with the name category, and each name row has an ID number, 
      # if a row has no ID number then it must belong to the same internal ID as the row directly above it
      database.red[i,"internal_id"] <- database.red[i-1,"internal_id"]
    }
  } 
  
  # use the ID column to pivot the dataframe from long to wide format
  database.long <- tidyr::pivot_wider(database.red,id_cols="internal_id",names_from="variable",values_from="value")
  
  ## return formatted reduced database
  return(as.data.frame(database.long))
}

#### plot all the matches for all the compounds annotated in eRah ####
plot_all_spectra <- function(Experiment, id_list = NULL, cutoff=0.8, compare = T,
                             id.database, draw.color = "purple", xlimit = NULL,
                             file_path, file_format="png",file_name_pattern="_compare_spectra",RI_dataframe=NULL){
  if(is.null(id_list)) id_df <- idList(Experiment,id.database=combo_lib)
  else id_df <- id_list

  for(i in id_df$AlignID){
    if(file_format=="png"){
      if(id_df[id_df$AlignID==i,"MatchFactor.1"] > cutoff){ # currently doesn't work but actually I don't want to use this argument yet
        png(file = paste0(file_path,"/","AlignID",i,"_",file_name_pattern,".png"),width=12, height=10, units="in",res=300)
        par(mfrow=c(2,2))
        plotSpectra(Experiment,id.database=id.database,AlignId=i,n.putative = 1,compare = compare,
                    draw.color = draw.color, xlim = xlimit,) 
        plotSpectra(Experiment,id.database=id.database,AlignId=i,n.putative = 2,compare = compare,
                    draw.color = draw.color, xlim = xlimit,) 
        plotSpectra(Experiment,id.database=id.database,AlignId=i,n.putative = 3,compare = compare,
                    draw.color = draw.color, xlim = xlimit,)
        plot.new()
        text(x=0.5,y=0.8,paste("Align ID",i))
        if(!is.null(RI_dataframe)){
          text(x=0.5,y=0.65,paste("Measured RI:",RI_dataframe[RI_dataframe$AlignID==i & RI_dataframe$MatchNumber==1,"compound_RI"]))
          text(x=0.5,y=0.5,paste("RI Match 1:",RI_dataframe[RI_dataframe$AlignID==i & RI_dataframe$MatchNumber==1,"annotation_RI"],collapse="/"))
          text(x=0.5,y=0.35,paste("RI Match 2:",RI_dataframe[RI_dataframe$AlignID==i & RI_dataframe$MatchNumber==2,"annotation_RI"],collapse="/"))
          text(x=0.5,y=0.2,paste("RI Match 3:",RI_dataframe[RI_dataframe$AlignID==i & RI_dataframe$MatchNumber==3,"annotation_RI"],collapse="/"))
        }
        dev.off()
      }
    }
  }
}

# Calculate fold change and p-values for each metabolite in a feature table, metabolites in rows, samples in columns
volcano_plot_calc <- function(data,group1cols,group2cols){
  # data: df with metabolite names as row names, one metabolite per row, one column per sample
  # group1cols and group2cols: vector of the column names of all columns belonging to group 1 or group 2, respectively
  out <- as.data.frame(matrix(nrow=nrow(data),ncol=5))
  colnames(out) <- c("metabolite","foldchange","log2foldchange","pvalue","neglog10pvalue")
  for(i in 1:nrow(data)){
    out[i,"metabolite"] <- rownames(data)[i]
    # calculate fold change and log2foldchange with gtools
    out[i,"foldchange"] <- gtools::foldchange(mean(data[i,group1cols]),mean(data[i,group2cols]))
    out[i,"log2foldchange"] <- gtools::foldchange2logratio(out[i,"foldchange"])
    
    # calculate the p-value from the t.test
    ttest <- t.test(data[i,group1cols],(data[i,group2cols]))
    out[i,"pvalue"] <- ttest$p.value
    
    # calculate the negative log10 of the p-value
    out[i,"neglog10pvalue"] <- -1*log10(out[i,"pvalue"])
  }
  return(out)
}
